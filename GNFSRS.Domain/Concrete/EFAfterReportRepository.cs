﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using System.Data.Entity;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.Domain.Concrete
{
    public class EFAfterReportRepository : IAfterReportRepository
    {
        private EFDbContext dbContext = new EFDbContext();

        public IQueryable<AfterReport> GetAfterReports(string reportCode)
        {
            return dbContext.AfterReports.Where(r => r.report_Code.StartsWith(reportCode));
        }


        public AfterReport GetAfterReport(int id)
        {
            return dbContext.AfterReports.Find(id);
        }

        // Return a collection of AfterReports
        public IQueryable<AfterReport> AfterReports
        {
            get { return dbContext.AfterReports; }
        }


        public void Save_After_Report(AfterReport afterReport)
        {
            if (afterReport.after_ReportID == 0)
            {
                dbContext.AfterReports.Add(afterReport);
            }
            else
            {
                //AfterReport dbEntry = dbContext.AfterReports.Find(afterReport.after_ReportID);
                if (afterReport != null)
                {
                    dbContext.AfterReports.Attach(afterReport);
                    dbContext.Entry(afterReport).State = EntityState.Modified;
                }

            }

            dbContext.SaveChanges();
        }

        public AfterReport Delete_After_Report(int id)
        {
            AfterReport dbEntry = dbContext.AfterReports.Find(id);
            if (dbEntry != null)
            {
                dbContext.AfterReports.Remove(dbEntry);
                dbContext.SaveChanges();
            }
            return dbEntry;
        }


        public string Duration_Of_Event(string ReportCode)
        {
            //TimeSpan time_d;

            //DateTime before_report_time;
            //DateTime return_time;

            StringBuilder get_time = new StringBuilder();
            var _before_report_time = dbContext.BeforeReports.Where(b => b.report_Code == ReportCode).Select(b => b.tender_Depature_Time);
            foreach (var bt in _before_report_time)
            {
                get_time.Append(bt.ToString());
            }

            StringBuilder get_time_after = new StringBuilder();
            var _return_time = dbContext.AfterReports.Where(a => a.report_Code == ReportCode).Select(a => a.return_time);
            foreach (var at in _return_time)
            {
                get_time_after.Append(at.ToString());
            }



            var duration = DateTime.Parse(get_time_after.ToString()) - DateTime.Parse(get_time.ToString()); //(return_time.Replace("AM", "").Replace("PM", "")); //- DateTime.Parse(before_report_time.Replace("AM", "").Replace("PM", ""));

            return duration.ToString();
        }


        /// <summary>
        /// server side jquery datatable retrieval
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortedColumns"></param>
        /// <param name="totalRecordCount"></param>
        /// <param name="searchRecordCount"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>

        public IList<AfterReport> GetAfterReports(int startIndex, int pageSize,
            ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount,
            out int searchRecordCount, string searchString = "")
        {
            IList<AfterReport> aReports = dbContext.AfterReports.ToList();

            totalRecordCount = aReports.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                aReports = dbContext.AfterReports.Where(
                    c => c.report_Code.ToLower().Contains(searchString.ToLower())
                    || c.status.ToLower() == (searchString.ToLower()) || c.cause_of_fire.ToLower().Trim()
                    .Contains(searchString.ToLower().Trim()) ).ToList();
            }

            searchRecordCount = aReports.Count();

            IList<AfterReport> sortedAfterReports = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "Report Code":
                        sortedAfterReports = sortedAfterReports == null ? aReports.OrderBy(e => e.report_Code).ToList()
                            : ((sortedColumn.Direction == SortingDirection.Ascending) ?
                            sortedAfterReports.OrderBy(e => e.report_Code).ToList() : sortedAfterReports.OrderByDescending(e => e.report_Code).ToList());
                        break;
                    case "Status":
                        sortedAfterReports = sortedAfterReports == null ? aReports.OrderBy(e => e.status).ToList()
                            : ((sortedColumn.Direction == SortingDirection.Ascending) ?
                            sortedAfterReports.OrderBy(e => e.status).ToList() : sortedAfterReports.OrderByDescending(e => e.status).ToList());
                        break;

                }
            }
            return sortedAfterReports.Skip(startIndex).Take(pageSize).ToList();
        }


    }
}
