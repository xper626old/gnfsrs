﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GNFSRS.Domain.Abstract;
using GNFSRS.Domain.Entities;

namespace GNFSRS.Domain.Concrete
{
    public class EFArchiveRepository : IArchiveRepository
    {
        private EFDbContext dbContext = new EFDbContext();
        IQueryable<Archive> IArchiveRepository.Archives
        {
            get { return dbContext.Archives; }
        }

        public Archive GetArchive(int id)
        {
            return dbContext.Archives.Find(id);
        }

        //void IArchiveRepository.Create_Archive(Archive archive)
        //{
        //    throw new NotImplementedException();
        //}

        public void Save_Archive(Archive archive)
        {
            if (archive.archiveID == 0)
            {
                dbContext.Archives.Add(archive);
            }
            else
            {
                Archive dbEntry = dbContext.Archives.Find(archive.archiveID);
                if (dbEntry != null)
                {
                    dbContext.Archives.Attach(dbEntry);
                    dbContext.Entry(dbEntry).State = EntityState.Modified;
                }
            }
            dbContext.SaveChanges();

        }

        public Archive Delete_Archive(int id)
        {
            Archive dbEntry = dbContext.Archives.Find(id);
            if (dbEntry != null)
            {
                dbContext.Archives.Remove(dbEntry);
                dbContext.SaveChanges();
            }
            return dbEntry;
        }

        public void Move_Reports_to_Archive(string datec)
        {
            int dateParam;
            DateTime DateChecker; 

            IEnumerable<AfterReport> afterReports = dbContext.AfterReports;

            IEnumerable<BeforeReport> beforeReports = dbContext.BeforeReports;

            if (datec == null)
            {
                int thisYear = DateTime.Now.Year;
                int lastYear = thisYear - 1;
                dateParam = lastYear;
            }
            else
            {
                 DateChecker = DateTime.Parse(datec);
                 dateParam = DateChecker.Year;
            }


            //var QueryBeforeReports = beforeReports.Where(b => b.before_ReportDate == dateParam).Select(b => b.report_Code)
            //    .Where(afterReports.Select(a => a).SkipWhile( r => r.report_Code).SkipWhile(r => r.after_ReportID))  );

            var Dbcontext = new EFDbContext();

            var QueryBeforeReports = (from bf in beforeReports
                                      join af in afterReports on bf.report_Code equals af.report_Code
                                      where bf.before_ReportDate.Year <= dateParam
                                      select new
                                      {
                                          afReports = af,
                                          bfReports = bf
                                      });


            Archive archive = new Archive();
            DeleteArchived deletArchived=new DeleteArchived();
            foreach (var r in QueryBeforeReports)
            {
              
               

                

                //foreach (var rp in r.bfReports as IEnumerable<BeforeReport>)
                //{

                    archive.report_Code = r.bfReports.report_Code;
                    archive.before_Name = r.bfReports.before_Name;
                    archive.before_Phone = r.bfReports.before_Phone;
                    archive.before_Report_Comments = r.bfReports.before_Report_Comments;
                    archive.before_ReportDate = r.bfReports.before_ReportDate;
                    archive.before_ReportTime = r.bfReports.before_ReportTime;
                    archive.before_Venue = r.bfReports.before_Venue;
                    archive.tender_Depature_Time = r.bfReports.tender_Depature_Time;




                    //foreach (var rp2 in r.afReports as IEnumerable<AfterReport>)
                    //{

                    archive.number_of_casualties = r.afReports.number_of_casualties;
                    archive.properties_lost = r.afReports.properties_lost;
                    archive.return_time = r.afReports.return_time;
                    archive.status = r.afReports.status;
                    archive.type_of_fire = r.afReports.type_of_fire;
                    archive.before_ReportID = r.bfReports.before_ReportID;


                    deletArchived.after_ReportID = r.afReports.after_ReportID;
                    deletArchived.before_ReportID = r.bfReports.before_ReportID;
                        // Pass the data to be saved
                    Dbcontext.Archives.Add(archive);
                    Dbcontext.SaveChanges();

                    Delete_Archived_Records(deletArchived);
            }


        }

       public bool Delete_Archived_Records(DeleteArchived deletArchived)
        {

            EFDbContext dbContext = new EFDbContext();

            var br  = dbContext.BeforeReports.Find(deletArchived.before_ReportID);
            var ar  = dbContext.AfterReports.Find(deletArchived.after_ReportID);

            dbContext.BeforeReports.Remove(br);

            dbContext.AfterReports.Remove(ar);

            dbContext.SaveChanges();
            return true;
        }

       

        
    }
}
