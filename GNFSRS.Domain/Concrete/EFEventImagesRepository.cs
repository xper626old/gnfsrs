﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using System.Data;

namespace GNFSRS.Domain.Concrete
{
    public class EFEventImagesRepository : IEventImagesRepository
    {
        private EFDbContext dbContext = new EFDbContext();



        public IQueryable<EventImage> EventImages
        {
            get { return dbContext.EventImages; }
        }

        public EventImage GetEventImages(int id)
        {
            return dbContext.EventImages.Find(id);
        }

        //public void Add_EventImages(EventImages eventImages)
        //{
        //    throw new NotImplementedException();
        //}

        public void Save_Image_Path(EventImage eventImages)
        {
            //if (eventImages.imageID == 0)
            //{
                dbContext.EventImages.Add(eventImages);
                dbContext.SaveChanges();
            //}
            //else
            //{
            //    EventImage dbEntry = dbContext.EventImages.Find(eventImages.imageID);
            //    if (dbEntry != null)
            //    {
            //        dbContext.EventImages.Attach(eventImages);
            //        dbContext.Entry(eventImages).State = EntityState.Modified;
            //    }
            //}
        }

        public EventImage Delete_Image_Path(int id)
        {
            EventImage dbEntry = dbContext.EventImages.Find(id);
            if (dbEntry != null)
            {
                dbContext.EventImages.Remove(dbEntry);
                dbContext.SaveChanges();
            }
            return dbEntry;
        }
    }
}
