﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;
using GNFSRS.Domain.Entities;


namespace GNFSRS.Domain.Concrete
{
    public  class EFDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<BeforeReport> BeforeReports { get; set; }
        public DbSet<AfterReport> AfterReports { get; set; }
        public DbSet<EventImage> EventImages {get; set;}
        public DbSet<Archive> Archives { get; set; }
        public DbSet<Years> Years {get; set;}

        public DbSet<Audit> AuditRecords { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }

        public DbSet<GNFSRSRole> GNFSRSRoles { get; set; }
    }
}
