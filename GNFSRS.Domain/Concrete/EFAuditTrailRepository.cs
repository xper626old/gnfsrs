﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using System.Data.Entity;
using GNFSRS.Domain.JQueryDataTable;
namespace GNFSRS.Domain.Concrete
{
    public class EFAuditTrailRepository : IAuditTrailRepository
    {
        private EFDbContext dbContext = new EFDbContext();

        public AuditTrail GetAuditTrail(int id)
        {
            return dbContext.AuditTrails.Find(id);
        }

        public IQueryable<AuditTrail> AuditTrails
        {
            get { return dbContext.AuditTrails; }
        }

        public void SaveAuditTrail(AuditTrail AuditTrail)
        {
            if (AuditTrail.AuditTrailID == 0)
            {
                dbContext.AuditTrails.Add(AuditTrail);
            }
            else
            {
                dbContext.AuditTrails.Attach(AuditTrail);
                dbContext.Entry(AuditTrail).State = EntityState.Modified;
            }

            dbContext.SaveChanges();
        }

        public void DeleteAuditTrail(AuditTrail AuditTrail)
        {
            dbContext.AuditTrails.Remove(AuditTrail);
            dbContext.SaveChanges();
        }

        public IEnumerable<AuditTrail> UserActivityWithinPeriod(CPeriod period)
        {
           
            return dbContext.AuditTrails
                .Where(d => d.ActivityDate >= period.StartDate
                && d.ActivityDate <= period.EndDate);
           
        }
    }
}
