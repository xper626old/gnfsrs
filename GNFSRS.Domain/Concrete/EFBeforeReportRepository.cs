﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using GNFSRS.Domain.JQueryDataTable;


namespace GNFSRS.Domain.Concrete
{
    public class EFBeforeReportRepository : IBeforeReportRepository
    {
        private EFDbContext dbContext = new EFDbContext();

        public IQueryable<BeforeReport> GetBeforeReports(string reportCode)
        {
            return dbContext.BeforeReports.Where(r => r.report_Code.StartsWith(reportCode)); 
        }

        public IQueryable<BeforeReport> BeforeReports
        {
            get { return dbContext.BeforeReports; }
        }

        public BeforeReport GetBeforeReport(int id)
        {
            return dbContext.BeforeReports.Find(id);
        }

        public void Save_Before_Report(BeforeReport beforeReport)
        {
            if (beforeReport.before_ReportID == 0)
            {
                dbContext.BeforeReports.Add(beforeReport);
            }
            else
            {
                //BeforeReport dbEntry = dbContext.BeforeReports.Find(beforeReport.before_ReportID);
                if (beforeReport != null)
                {
                    dbContext.BeforeReports.Attach(beforeReport);
                    dbContext.Entry(beforeReport).State = EntityState.Modified;
                }
            }

            dbContext.SaveChanges();
        }

        public BeforeReport Delete_Before_Report(int id)
        {
            BeforeReport dbEntry = dbContext.BeforeReports.Find(id);
            if (dbEntry != null)
            {
                dbContext.BeforeReports.Remove(dbEntry);
                dbContext.SaveChanges();
            }
            return dbEntry;
        }


        /// <summary>
        /// server side jquery datatable retrieval
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortedColumns"></param>
        /// <param name="totalRecordCount"></param>
        /// <param name="searchRecordCount"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>

        public IList<BeforeReport> GetBeforeReports(int startIndex, int pageSize,
           ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount,
           out int searchRecordCount, string searchString = "")
        {
            IList<BeforeReport> bReports = dbContext.BeforeReports.ToList();

            totalRecordCount = bReports.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                bReports = dbContext.BeforeReports.Where(
                    c => c.report_Code.ToLower().Contains(searchString.ToLower())
                    ||  c.before_ReportDate == DateTime.Parse(searchString.ToLower())).ToList();
            }

            searchRecordCount = bReports.Count();

            IList<BeforeReport> sortedBeforeReports = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "Report Code":
                        sortedBeforeReports = sortedBeforeReports == null ? bReports.OrderBy(e => e.report_Code).ToList()
                            : ((sortedColumn.Direction == SortingDirection.Ascending) ?
                            sortedBeforeReports.OrderBy(e => e.report_Code).ToList() : sortedBeforeReports.OrderByDescending(e => e.report_Code).ToList());
                        break;
                    case "Report Date":
                        sortedBeforeReports = sortedBeforeReports == null ? bReports.OrderBy(e => e.before_ReportDate).ToList()
                            : ((sortedColumn.Direction == SortingDirection.Ascending) ?
                            sortedBeforeReports.OrderBy(e => e.before_ReportDate).ToList() : sortedBeforeReports.OrderByDescending(e => e.before_ReportDate).ToList());
                        break;
                   
                }
            }
            return sortedBeforeReports.Skip(startIndex).Take(pageSize).ToList();
        }

        
    }
}
