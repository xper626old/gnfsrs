﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GNFSRS.Domain.Abstract;
using GNFSRS.Domain.Entities;

namespace GNFSRS.Domain.Concrete
{
    public class EFUserRepository : IUserRepository
    {
        private EFDbContext dbContext = new EFDbContext();
        public IQueryable<User> Users
        {
            get { return dbContext.Users; }
        }

        public User GetUser(int id)
        {
            return dbContext.Users.Find(id);
        }

        public void Save_User(User user)
        {
            if (user.userID == 0)
            {
                dbContext.Users.Add(user);
            }
            else
            {
                User dbEntry = dbContext.Users.Find(user.userID);
                if (dbEntry != null)
                {
                    dbContext.Users.Attach(dbEntry);
                    dbContext.Entry(dbEntry).State = EntityState.Modified;
                }
            }
            dbContext.SaveChanges();
        }

        public User Delete_User(int id)
        {
            User dbEntry = dbContext.Users.Find(id);
            if (dbEntry != null)
            {
                dbContext.Users.Remove(dbEntry);
                dbContext.SaveChanges();
            }
            return dbEntry;

        }
    }
}
