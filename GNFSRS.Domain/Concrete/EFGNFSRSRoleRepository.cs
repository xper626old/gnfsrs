﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using System.Data.Entity;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.Domain.Concrete
{
    //public class RoleDataContext : DbContext
    //{
    //    public RoleDataContext()
    //        : base("name=DefaultConnection")
    //    {
    //    }
    //}

   public class EFGNFSRSRoleRepository : IGNFSRSRoleRepository
    {

       //private RoleDataContext context = new RoleDataContext();

       private EFDbContext context = new EFDbContext();
        public GNFSRSRole GetGNFSRSRole(int id)
        {
            return context.GNFSRSRoles.Find(id);
            
        }



        public IQueryable<GNFSRSRole> GNFSRSRoles
        {
            get { return context.GNFSRSRoles; }
        }



        public void SaveGNFSRSRole(GNFSRSRole GNFSRSRole)
        {
            if (GNFSRSRole.GNFSRSRoleID == 0)
            {
                context.GNFSRSRoles.Add(GNFSRSRole);
            }
            else
            {
                context.GNFSRSRoles.Attach(GNFSRSRole);
                context.Entry(GNFSRSRole).State = EntityState.Modified;
            }

            context.SaveChanges();
        }

        public void DeleteGNFSRSRole(GNFSRSRole GNFSRSRole)
        {
            context.GNFSRSRoles.Remove(GNFSRSRole);
            context.SaveChanges();
        }


    }
}
