﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;

namespace GNFSRS.Domain.Abstract
{
    public interface IUserRepository
    {
        IQueryable<User> Users { get; }

        User GetUser(int id);

        void Save_User(User user);

        User Delete_User(int id);
    }
}
