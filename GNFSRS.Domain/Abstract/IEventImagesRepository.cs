﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;

namespace GNFSRS.Domain.Abstract
{
    public interface IEventImagesRepository
    {
        IQueryable<EventImage> EventImages { get; }

        EventImage GetEventImages(int id);

        //Creating a new image instance
        //void Add_EventImages(EventImages eventImages);

        void Save_Image_Path(EventImage eventImages);

        EventImage Delete_Image_Path(int id);

        //void Edit_Image_Path(int id);

       

    }
}
