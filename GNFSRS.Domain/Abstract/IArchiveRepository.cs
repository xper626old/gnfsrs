﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;

namespace GNFSRS.Domain.Abstract
{
    public interface IArchiveRepository
    {
        IQueryable<Archive> Archives { get; }

        Archive GetArchive(int id);

        //void Create_Archive(Archive archive);

        void Save_Archive(Archive archive);

        Archive Delete_Archive(int id);

        void Move_Reports_to_Archive(string DateChecker);

        bool Delete_Archived_Records(DeleteArchived deletArchived);
    }
}
