﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.Domain.Abstract
{
    public interface IBeforeReportRepository
    {
        IQueryable<BeforeReport> GetBeforeReports(string reportCode);

        IQueryable<BeforeReport> BeforeReports { get; }

        BeforeReport GetBeforeReport(int id);

        //void Create_Before_Report( BeforeReport beforeReport);

        //void Edit_Before_Report(int id);

        void Save_Before_Report(BeforeReport beforeReport);

        BeforeReport Delete_Before_Report(int id);

        IList<BeforeReport> GetBeforeReports(int startIndex, int pageSize,
          System.Collections.ObjectModel.ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount,
          out int searchRecordCount, string searchString);


    }
}
