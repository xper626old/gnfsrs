﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.Domain.Abstract
{
    public interface IAfterReportRepository
    {
        IQueryable<AfterReport> GetAfterReports(string reportCode);

        IQueryable<AfterReport> AfterReports { get; }

        AfterReport GetAfterReport(int id);

        //void Create_After_Report(AfterReport afterReport);

        void Save_After_Report(AfterReport afterReport);

        //void Edit_After_Report(int id);

        AfterReport Delete_After_Report(int id);

        // Calculate the time spent on a particular event.
        string Duration_Of_Event(string ReportCode);

        IList<AfterReport> GetAfterReports(int startIndex, int pageSize,
         System.Collections.ObjectModel.ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount,
         out int searchRecordCount, string searchString);

    }
}
