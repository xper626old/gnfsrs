﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.Domain.Abstract
{

    public interface IAuditTrailRepository
    {
        AuditTrail GetAuditTrail(int id);
        void DeleteAuditTrail(AuditTrail AuditTrail);
        IQueryable<AuditTrail> AuditTrails { get; }
        void SaveAuditTrail(AuditTrail AuditTrail);

        IEnumerable<AuditTrail> UserActivityWithinPeriod(CPeriod period);
    }
}
