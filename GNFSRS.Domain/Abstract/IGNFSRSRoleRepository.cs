﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.JQueryDataTable;


namespace GNFSRS.Domain.Abstract
{
    public interface IGNFSRSRoleRepository
    {
        GNFSRSRole GetGNFSRSRole(int id);
        void DeleteGNFSRSRole(GNFSRSRole GNFSRSRole);
        IQueryable<GNFSRSRole> GNFSRSRoles { get; }
        void SaveGNFSRSRole(GNFSRSRole GNFSRSRole);
    }
}
