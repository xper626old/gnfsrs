﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class ReportGenerator
    {
        private DateTime currentDate;
        private TimeSpan currentTime;

        public DateTime ReportDate
        {
            get { return currentDate.Date; }
           
        }
        public DateTime ReportTime
        {
            get { return DateTime.Now.ToLocalTime(); }
            
        }
    }
}
