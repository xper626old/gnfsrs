﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class User
    {
        [Key]
        public int userID { get; set; }

        public string userCode { get; set; }

        public string first_Name { get; set; }

        public string last_Name { get; set; }

        public string user_Rank { get; set; }

        public string user_Address { get; set; }

        public string user_Phone { get; set; }

        public string user_Email { get; set; }


    }
}
