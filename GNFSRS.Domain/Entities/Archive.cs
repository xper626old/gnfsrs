﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class Archive
    {
        [Key]
        public int archiveID { get; set; }

        public int before_ReportID { get; set; }

        public int after_ReportID { get; set; }

        public string report_Code { get; set; }

        public DateTime before_ReportDate { get; set; }

        public DateTime before_ReportTime { get; set; }

        public string before_Venue { get; set; }

        public string before_Name { get; set; }

        public string before_Phone { get; set; }

        public DateTime tender_Depature_Time { get; set; }

        public string before_Report_Comments { get; set; }

        public string type_of_fire { get; set; }

        public DateTime return_time { get; set; }

        public int number_of_casualties { get; set; }

        public string properties_lost { get; set; }

        public string status { get; set; }
        
    }

    public class DeleteArchived
    {
        public int before_ReportID { get; set; }

        public int after_ReportID { get; set; }
    }
}
