﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
   public class BeforeReport
    {
       private DateTime formatDate;
       [Key]
       public int before_ReportID { get; set; }

       [Required]
       [Display(Name="Report Code",Prompt="Enter a Unique Code For every Report, Event Name and Date")]
       public string report_Code { get; set; }

       [Required, DataType(DataType.Date)]
       [Display(Name = "Report Date")]
       [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
       public DateTime before_ReportDate
       {
           get;
           set;
       }

       [Required, DataType(DataType.Time)]
       [Display(Name="Report Time")]
       public DateTime before_ReportTime { get; set; }

       [Required]
       [Display(Name = "Report Venue")]
       public string before_Venue { get; set; }

       [Required]
       [Display(Name = "Report Name")]
       public string before_Name { get; set; }

       [DataType(DataType.PhoneNumber)]
       [Display(Name = "Phone", Prompt="Phone Number of reporter")]
       public string before_Phone { get; set; }

       [Required, DataType(DataType.Time)]
       [Display(Name = "Depature Time")]
       public DateTime tender_Depature_Time { get; set; }

       [Display(Name = "Comments")]
       public string before_Report_Comments { get; set; }

       public string Category { get; set; }

       public string BeforeDate() {

           var r = before_ReportDate.Year + "-" + before_ReportDate.Month + "-" + before_ReportDate.Day;
           return r;
       }
    }
}
