﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace GNFSRS.Domain.Entities
{
   public class Years
    {
       [Key]
       public int id { get; set; }
       public int Year { get; set; }
    }
}
