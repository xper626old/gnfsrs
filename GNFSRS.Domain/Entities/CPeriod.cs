﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace GNFSRS.Domain.Entities
{
    public class CPeriod
    {
        private DateTime _thisDate;
        private DateTime _startDate;
        private DateTime _endDate;

        public enum Periods
        {
            Today = 1, Yesterday, LastWeek, ThisMonth, LastMonth, LastQuarter, LastYear, ThisYear, Custom
        }

        public DateTime StartDate
        {
            get
            {
                return _startDate;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
        }


        public CPeriod(DateTime in_date)
        {
            _thisDate = in_date;
            _startDate = _thisDate;
            _endDate = _thisDate;

        }

        public void SetCustomPeriod(DateTime sDate, DateTime eDate)
        {
            _startDate = sDate;
            if (eDate.CompareTo(sDate) >= 0)
                _endDate = eDate;
            else
                _endDate = sDate;
        }

        public void SelectPeriod(int myPeriod)
        {
            IFormatProvider culture = new CultureInfo("fr-FR", true);
            string dateString = "";

            switch (myPeriod)
            {
                case (int)Periods.Today:
                    break;
                case (int)Periods.Yesterday:
                    _startDate = _thisDate.AddDays(-1);
                    _endDate = _startDate;
                    break;
                case (int)Periods.LastWeek:
                    #region determine the day of week
                    switch (_thisDate.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            _startDate = _thisDate.AddDays(-7);
                            break;
                        case DayOfWeek.Monday:
                            _startDate = _thisDate.AddDays(-8);
                            break;
                        case DayOfWeek.Tuesday:
                            _startDate = _thisDate.AddDays(-9);
                            break;
                        case DayOfWeek.Wednesday:
                            _startDate = _thisDate.AddDays(-10);
                            break;
                        case DayOfWeek.Thursday:
                            _startDate = _thisDate.AddDays(-11);
                            break;
                        case DayOfWeek.Friday:
                            _startDate = _thisDate.AddDays(-12);
                            break;
                        case DayOfWeek.Saturday:
                            _startDate = _thisDate.AddDays(-13);
                            break;
                    }
                    #endregion
                    _endDate = _startDate.AddDays(6);
                    break;
                case (int)Periods.ThisMonth:
                    dateString = "01/" + Convert.ToString(_thisDate.Month) +
                                                "/" + _thisDate.Year.ToString();
                    _startDate = DateTime.Parse(dateString, culture);
                    _endDate = _startDate.AddDays(
                        DateTime.DaysInMonth(_startDate.Year, _startDate.Month) - 1);
                    break;
                case (int)Periods.LastMonth:
                    dateString = "01/" + Convert.ToString(_thisDate.Month - 1) +
                                                "/" + _thisDate.Year.ToString();
                    _startDate = DateTime.Parse(dateString, culture);
                    _endDate = _startDate.AddDays(
                        DateTime.DaysInMonth(_startDate.Year, _startDate.Month) - 1);
                    break;
                case (int)Periods.LastQuarter:
                    _startDate = _thisDate.AddMonths(-3);
                    _endDate = _thisDate;
                    break;
                case (int)Periods.LastYear:
                    dateString = "01/01/" + Convert.ToString(_thisDate.Year - 1);
                    _startDate = DateTime.Parse(dateString, culture);
                    dateString = "31/12/" + Convert.ToString(_thisDate.Year - 1);
                    _endDate = DateTime.Parse(dateString, culture);
                    break;
                case (int)Periods.ThisYear:
                    dateString = "01/01/" + _thisDate.Year.ToString();
                    _startDate = DateTime.Parse(dateString, culture);
                    dateString = "31/12/" + Convert.ToString(_thisDate.Year);
                    _endDate = DateTime.Parse(dateString, culture);
                    break;
                default:
                    break;
            }
        }
    }
}
