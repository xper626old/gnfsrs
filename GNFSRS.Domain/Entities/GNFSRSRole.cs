﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class GNFSRSRole
    {
        public int GNFSRSRoleID { get; set; }
        [Required]
        public string RoleName { get; set; }
    }

    public class UserRole
    {
        public int UserRoleID { get; set; }
        [Required]
        public int GNFSRSRoleID { get; set; }
        [Required]
        public int userID { get; set; }

        public ICollection<GNFSRSRole> GNFSRSRoles { get; set; }
    }

}
