﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class EventImage
    {
        [Key]
        public int imageID { get; set; }
        public string report_Code { get; set; }
        public string image_path { get; set; }
    }
}
