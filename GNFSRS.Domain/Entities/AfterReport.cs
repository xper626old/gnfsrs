﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.Domain.Entities
{
    public class AfterReport
    {
        [Key]
        public int after_ReportID { get; set; }

        [Required]
        [Display(Name = "Report Code", Prompt = "Enter a Unique Code For every Report, Event Name and Date")]
        public string report_Code {get; set;}

        [Required]
        [Display(Name = "Type of Fire")]
        public string type_of_fire { get; set; }

        [Required, DataType(DataType.Time)]
        [Display(Name = "Return Time")]
        public DateTime return_time { get; set; }

        [Display(Name = "Number of Casulaties")]
        public int number_of_casualties { get; set; }

        [Display(Name = "Properties Lost")]
        public string properties_lost { get; set; }

        [Display(Name = "Status")]
        public string status { get; set; }

        [Display(Name = "Cause of Fire")]
        public string cause_of_fire { get; set; }


    }
}
