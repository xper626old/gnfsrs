﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GNFSRS.Domain.Entities
{
   public class Audit
    {
       public int ID { get; set; }
        //A new SessionId that will be used to link an entire users "Session" of Audit Logs
        //together to help identifer patterns involving erratic behavior
        public string SessionID { get; set; }
        public Guid AuditID { get; set; }
        public string IPAddress { get; set; }
        public string UserName { get; set; }
        public string URLAccessed { get; set; }
        public DateTime TimeAccessed { get; set; }
        //A new Data property that is going to store JSON string objects that will later be able to
        //be deserialized into objects if necessary to view details about a Request
        public string Data { get; set; }

        public Audit()
        {
        }

    }

    public class AuditTrail
    {
        [Key, HiddenInput(DisplayValue = false)]
        public int AuditTrailID { get; set; }

        [Required, Display(Name = "User Code ")]
        public string userCode { get; set; }

        public string Activity { get; set; }

        public int userID { get; set; }

        public string UserName { get; set; }

        [Required, Display(Name = "Activity Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ActivityDate { get; set; }

        public string Data { get; set; }
    }
}
