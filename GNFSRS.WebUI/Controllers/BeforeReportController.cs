﻿using System;
using System.Collections.Generic;
using System.Data;
using PagedList;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.Abstract;
using GNFSRS.WebUI.Models;
using GNFSRS.Domain.JQueryDataTable;
using GNFSRS.WebUI.Filters;

namespace GNFSRS.WebUI.Controllers
{
    public class BeforeReportController : Controller
    {
        private IBeforeReportRepository db;

        //int PageSize = 2;

        public BeforeReportController(IBeforeReportRepository repo)
        {
            db = repo;
        }

        ////
        //// GET: /BeforeReport/

        //public ActionResult Index(string category, int page = 1)
        //{
        //    //return View(db.BeforeReports.ToList());

        //    BeforeReportViewModel model = new BeforeReportViewModel
        //    {
        //        BeforeReports = db.BeforeReports
        //        .Where(p => category == null || p.Category == category)
        //        .OrderBy(p => p.before_ReportID)
        //        .Skip((page - 1) * PageSize)
        //        .Take(PageSize),
        //        PagingInfo = new PagingInfo
        //        {
        //            CurrentPage = page,
        //            ItemsPerPage = PageSize,
        //            TotalItems = category == null ?
        //            db.BeforeReports.Count() :
        //            db.BeforeReports.Where(e => e.Category == category).Count()
        //        },
        //        CurrentCategory = category
        //    };

        //    return View(model);
        //}


        [HttpGet]
        public ActionResult Search()
        {
            return View("Search");
        }

        [HttpPost]
        public JsonResult Search(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount;
            int searchRecordCount;
            var beforeReports = db.GetBeforeReports(
                startIndex: jQueryDataTablesModel.iDisplayStart,
                pageSize: jQueryDataTablesModel.iDisplayLength,
                sortedColumns: jQueryDataTablesModel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount,
                searchRecordCount: out searchRecordCount,
                searchString: jQueryDataTablesModel.sSearch);

            return Json(new { items = beforeReports, totalRecords = totalRecordCount, totalDisplayRecords = totalRecordCount, sEcho = 1 });
           

        }

        //
        // GET: /BeforeReports/Index/
        [Authorize]
        [Audit(AuditingLevel = 2)]  
        public ActionResult Index(string sort, string cT, string cO, string sT, string sO, int? page)
        {
            ViewBag.ReportCodeSortParam = string.IsNullOrEmpty(sort) ? "report_Code desc" : "";
            ViewBag.ReportDateSortParam = sort == "Date" ? "before_ReportDate_desc" : "Date";

            if (Request != null)
            {
                if (Request.HttpMethod == "GET")
                {
                    sT = cT;
                    sO = cO;
                }
                else
                {
                    page = 1;
                }
            }

            ViewBag.ShortInfoText = sT;
            ViewBag.OtherDetailsText = sO;

            IQueryable<BeforeReport> before_reports = db.BeforeReports;

            if (!string.IsNullOrEmpty(sT))
            {
                before_reports = before_reports.Where(e => e.before_Name.ToUpper().Contains(sT.ToUpper()) ||
                     e.report_Code.ToUpper().Contains(sT.ToUpper()));
            }

            if (!string.IsNullOrEmpty(sO))
            {
                // ***************************************************
                // Developer Nightmare encounterd
                // Linq cannot interpret DateTime object,
                // Thus must convert input to DateTime 
                // Before using in linq expression.
                // ****************************************************
                if (sO.Length == 10)
                {
                    var sDate =DateTime.Parse(sO);
                    before_reports = before_reports.Where(e => e.before_ReportDate == sDate);
                    //ViewBag.Date = sDate;
                }
                else if(sO.Length == 4)
                {
                    // Convert string to DateTime Year if length < 5
                    var sDate = DateTime.ParseExact(sO, "yyyy", null);
                    before_reports = before_reports.Where(e => e.before_ReportDate.Year == sDate.Year);
                  
                }
                    // Compare year and month
                else if (sO.Length == 7)
                {
                    var sDate = DateTime.Parse(sO);
                    before_reports = before_reports.Where(e => e.before_ReportDate.Year == sDate.Year && e.before_ReportDate.Month == sDate.Month);
                }

               // before_reports = before_reports.Where(e => e.before_ReportDate == ViewBag.Date); //.ToString().ToUpper().Contains(sO.ToUpper())
            }

            switch (sort)
            {
                case "report_Code desc":
                    before_reports = before_reports.OrderByDescending(e => e.report_Code);
                    break;
                case "Date":
                    before_reports = before_reports.OrderBy(e => e.before_ReportDate);
                    break;
                case "before_ReportDate_desc":
                    before_reports = before_reports.OrderByDescending(e => e.before_ReportDate);
                    break;
                default:
                    before_reports = before_reports.OrderBy(e => e.report_Code);
                    break;
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);

            return View(before_reports.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /BeforeReport/Details/5
        [Audit(AuditingLevel = 3)]  
        public ActionResult Details(int id = 0)
        {
            BeforeReport beforereport = db.GetBeforeReport(id);
            if (beforereport == null)
            {
                return HttpNotFound();
            }
            return View(beforereport);
        }

        //
        // GET: /BeforeReport/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BeforeReport/Create

        [HttpPost]
        public ActionResult Create(BeforeReport beforereport)
        {
            if (ModelState.IsValid)
            {
                db.Save_Before_Report(beforereport);
                return RedirectToAction("Index");
            }

            return View(beforereport);
        }

        //
        // GET: /BeforeReport/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BeforeReport beforereport = db.GetBeforeReport(id);
            if (beforereport == null)
            {
                return HttpNotFound();
            }
            return View(beforereport);
        }

        //
        // POST: /BeforeReport/Edit/5

        [HttpPost]
        public ActionResult Edit(BeforeReport beforereport)
        {
            if (ModelState.IsValid)
            {
                db.Save_Before_Report(beforereport);
                return RedirectToAction("Index");
            }
            return View(beforereport);
        }

        //
        // GET: /BeforeReport/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BeforeReport beforereport = db.GetBeforeReport(id);
            if (beforereport == null)
            {
                return HttpNotFound();
            }
            return View(beforereport);
        }

        //
        // POST: /BeforeReport/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {

            db.Delete_Before_Report(id);
            return RedirectToAction("Index");
        }



        //
        //AJAX GET:
        public JsonResult GetBeforeReportShortInfo(int id)
        {
            BeforeReport bfReport = db.GetBeforeReport(id);
            if (bfReport == null)
            {
                return Json(new
                {
                    reportCode = "Not Found",
                    reportName = "Not Found",
                    othernames = "Not Found",
                    reportDate = "Not Found",
                });
            }
            return Json(new
            {
                reportCode = bfReport.report_Code,
                reportName = bfReport.before_Name,
                reportDate = bfReport.before_ReportDate,
                reportComments = bfReport.before_Report_Comments
            }, JsonRequestBehavior.AllowGet);

        }

        //
        //AJAX GET:
        public JsonResult GetMatchingBeforeReports(string term)
        {
            var result = (from e in db.BeforeReports
                          orderby e.before_ReportID
                          where e.report_Code.StartsWith(term)
                          select new { BeforeReportID = e.before_ReportID, ReportCode = e.report_Code })
                          .Take(15).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}