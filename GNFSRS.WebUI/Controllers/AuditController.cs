﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.WebUI.Filters;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;

namespace GNFSRS.WebUI.Controllers
{
    [Authorize]
    public class AuditController : Controller
    {
        private EFDbContext db = new EFDbContext();

        //
        // GET: /Audit/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AuditRecords()
        {
            var audits = db.AuditRecords.OrderBy(a => a.TimeAccessed);
            return View(audits);
        }

        public ActionResult Details(int id)
        {
            Audit audit = db.AuditRecords.Find(id);
            if (audit == null)
            {
                return View("NotFound");
            }
            return View(audit);
        }
    }
}