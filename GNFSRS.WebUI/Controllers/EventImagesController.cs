﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.Abstract;
using GNFSRS.WebUI.Models;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Net;

namespace GNFSRS.WebUI.Controllers
{
    public class EventImagesController : Controller
    {
        private IEventImagesRepository db;

        public EventImagesController(IEventImagesRepository repo)
        {
            db = repo;
        }

        //
        // GET: /EventImages/

        public ActionResult Index()
        {
            var images = new EventImageViewModel();
            //Read out files from the files directory
            var files = Directory.GetFiles(Server.MapPath("~/Content/img"));
            //Add them to the model
            foreach (var file in files)
                images.Images.Add(Path.GetFileName(file));

            return View(images);
        }


        public ActionResult GetImageForEvent(string reportCode = null)
        {
            var eventImages = new EventImageViewModel();
            var eventImagesFiles = Directory.GetFiles(Server.MapPath("~/Content/img"));

            foreach (var file in eventImagesFiles)
            {
                if (reportCode != null)
                {
                    if (file.Contains(reportCode) == true)
                    {
                        eventImages.Images.Add(Path.GetFileName(file));
                    }
                }
              
            }

            return View(eventImages);
        }

        //
        // POST: /Home/PreviewImage

        [HttpPost]
        public ActionResult PreviewImage()
        {
            var bytes = new byte[0];
            ViewBag.Mime = "image/png";

            if (Request.Files.Count == 1)
            {
                bytes = new byte[Request.Files[0].ContentLength];
                Request.Files[0].InputStream.Read(bytes, 0, bytes.Length);
                ViewBag.Mime = Request.Files[0].ContentType;
            }

            ViewBag.Message = Convert.ToBase64String(bytes, Base64FormattingOptions.InsertLineBreaks);
            return PartialView();
        }

        //
        // GET: /Home/UploadImageModal

        public ActionResult UploadImageModal()
        {
            return View();
        }

     
        //
        // GET: /EventImages/Delete/5

        public ActionResult Delete(int id = 0)
        {
            EventImage eventimage = db.GetEventImages(id);
            if (eventimage == null)
            {
                return HttpNotFound();
            }
            return View(eventimage);
        }

        //
        // POST: /EventImages/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            
            db.Delete_Image_Path(id);
            return RedirectToAction("Index");
        }




        public ActionResult UploadImage()
        {
            //Just to distinguish between ajax request (for: modal dialog) and normal request
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }

            return View();
        }

        //
        // POST: /Home/UploadImage

        [HttpPost]
        public ActionResult UploadImage(EventImageViewModel model)
        {
            EventImage eventImage = new EventImage();

            //Check if all simple data annotations are valid
            if (ModelState.IsValid)
            {
                //Prepare the needed variables
                Bitmap original = null;
                var name = "newimagefile";
                var errorField = string.Empty;

               
                if (model.File != null) // model.IsFile
                {
                    errorField = "File";
                    name = Path.GetFileNameWithoutExtension(model.File.FileName);
                    original = Bitmap.FromStream(model.File.InputStream) as Bitmap;
                }

                //If we had success so far
                if (original != null)
                {
                    var img = CreateImage(original, model.X, model.Y, model.Width, model.Height);

                    // Save image in the file system
                    var fn = Server.MapPath("~/Content/img/"+ model.report_Code+ "-" + name + ".png");

                    //******************* Store Image Path in DB **********************//
                    eventImage.image_path = fn;
                    eventImage.report_Code = model.report_Code;
                    db.Save_Image_Path(eventImage);
                    // *****************************************************************//

                    // Flush Image to folder
                    img.Save(fn, System.Drawing.Imaging.ImageFormat.Png);

                    ViewBag.Success = "The Image has been uploaded successfully....";
                    return View();
                }
                else //Otherwise we add an error and return to the (previous) view with the model data
                    ModelState.AddModelError(errorField, "Your upload did not seem valid. Please try again using only correct images!");
                    ViewBag.Error = "Your upload did not seem valid. Please try again using only correct images!";
            }

            ViewBag.Error = "Your upload did not seem valid. Please try again using only correct images! and Enter Report Code";
            return View(model);
        }

        /// <summary>
        /// Creates a small image out of a larger image.
        /// </summary>
        /// <param name="original">The original image which should be cropped (will remain untouched).</param>
        /// <param name="x">The value where to start on the x axis.</param>
        /// <param name="y">The value where to start on the y axis.</param>
        /// <param name="width">The width of the final image.</param>
        /// <param name="height">The height of the final image.</param>
        /// <returns>The cropped image.</returns>
        Bitmap CreateImage(Bitmap original, int x, int y, int width, int height)
        {
            var img = new Bitmap(width, height);

            using (var g = Graphics.FromImage(img))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(original, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            }

            return img;
        }

      
    }
}