﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.Abstract;
using GNFSRS.WebUI.Models;

namespace GNFSRS.WebUI.Controllers
{
    public class ArchivesController : Controller
    {
        private IArchiveRepository db;

        public ArchivesController(IArchiveRepository repo)
        {
            db = repo;
        }

        //
        // GET: /Archives/

        public ActionResult Index()
        {
           //db.Move_Reports_to_Archive(DateTime.Now);

          
            return View(db.Archives.ToList());
        }

        //
        // GET: /Archives/Details/5

        public ActionResult Details(int id = 0)
        {
            Archive archive = db.GetArchive(id);
            if (archive == null)
            {
                return HttpNotFound();
            }
            return View(archive);
        }

        //
        // GET: /Archives/Create

        public ActionResult Create()
        {
            EFDbContext db = new EFDbContext();
            ViewBag.Years = new SelectList(db.Years, "Year", "Year");
            return View();
        }

        //
        // POST: /Archives/Create

        [HttpPost]
        public ActionResult Create( ArchiveViewModel avm)
        {
            
            
                if (avm.UseDefault == true)
                {
                    db.Move_Reports_to_Archive(null);
                }
                else if(avm.Year != null)
                {
                    var dt = DateTime.ParseExact(avm.Year.ToString(), "yyyy", null);
                    db.Move_Reports_to_Archive(dt.ToString());
                    return RedirectToAction("Index");
                }

            return RedirectToAction("Index");
        }

                
                

        

        //
        // GET: /Archives/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Archive archive = db.GetArchive(id);
            if (archive == null)
            {
                return HttpNotFound();
            }
            return View(archive);
        }

        //
        // POST: /Archives/Edit/5

        [HttpPost]
        public ActionResult Edit(Archive archive)
        {
            if (ModelState.IsValid)
            {
                db.Save_Archive(archive);
                return RedirectToAction("Index");
            }
            return View(archive);
        }

        //
        // GET: /Archives/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Archive archive = db.GetArchive(id);
            if (archive == null)
            {
                return HttpNotFound();
            }
            return View(archive);
        }

        //
        // POST: /Archives/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete_Archive(id);
            
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}