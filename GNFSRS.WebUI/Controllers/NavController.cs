﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.Abstract;
using GNFSRS.WebUI.Models;

namespace GNFSRS.WebUI.Controllers
{
    public class NavController : Controller
    {
        //
        // GET: /Nav/

        public ActionResult Menu()
        {
            return View();
        }

    }
}
