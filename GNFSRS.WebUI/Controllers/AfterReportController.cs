﻿using System;
using System.Collections.Generic;
using System.Data;
using PagedList;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using GNFSRS.Domain.Abstract;
using GNFSRS.WebUI.Models;
using GNFSRS.Domain.JQueryDataTable;

namespace GNFSRS.WebUI.Controllers
{
    public class AfterReportController : Controller
    {
        private IAfterReportRepository db;

        public AfterReportController(IAfterReportRepository repo)
        {
            db = repo;
        }

        //
        // GET: /AfterReport/

        //public ActionResult Index()
        //{
        //    ViewBag.duration = db.Duration_Of_Event("CodeE111");
        //    return View(db.AfterReports.ToList());
        //}


        [HttpGet]
        public ActionResult Search()
        {
            return View("Search");
        }

        [HttpPost]
        public JsonResult Search(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount;
            int searchRecordCount;
            var beforeReports = db.GetAfterReports(
                startIndex: jQueryDataTablesModel.iDisplayStart,
                pageSize: jQueryDataTablesModel.iDisplayLength,
                sortedColumns: jQueryDataTablesModel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount,
                searchRecordCount: out searchRecordCount,
                searchString: jQueryDataTablesModel.sSearch);

            return Json(new { items = beforeReports, totalRecords = totalRecordCount, totalDisplayRecords = totalRecordCount, sEcho = 1 });


        }

        //
        // GET: /BeforeReports/Index/
        [Authorize]
        public ActionResult Index(string sort, string cT, string cO, string sT, string sO, int? page)
        {
            // Test duration methods
            ViewBag.duration = db.Duration_Of_Event("CodeE111");

            ViewBag.ReportCodeSortParam = string.IsNullOrEmpty(sort) ? "report_Code_desc" : "";
            ViewBag.ReportDateSortParam = sort == "status" ? "status_desc" : "status";

            if (Request != null)
            {
                if (Request.HttpMethod == "GET")
                {
                    sT = cT;
                    sO = cO;
                }
                else
                {
                    page = 1;
                }
            }

            ViewBag.ShortInfoText = sT;
            ViewBag.OtherDetailsText = sO;

            IQueryable<AfterReport> after_reports = db.AfterReports;

            if (!string.IsNullOrEmpty(sT))
            {
                after_reports = after_reports.Where(e => e.cause_of_fire.ToUpper().Contains(sT.ToUpper()) ||
                     e.report_Code.ToUpper().Contains(sT.ToUpper()));
            }

            if (!string.IsNullOrEmpty(sO))
            {
                //// ***************************************************
                //// Developer Nightmare encounterd
                //// Linq cannot interpret DateTime object,
                //// Thus must convert input to DateTime 
                //// Before using in linq expression.
                //// ****************************************************
                //if (sO.Length > 5)
                //{
                //    var sDate = DateTime.Parse(sO);
                //    after_reports = after_reports.Where(e => e.before_ReportDate == sDate);
                //    //ViewBag.Date = sDate;
                //}
                //else
                //{
                //    // Convert string to DateTime Year if length < 5
                //    var sDate = DateTime.ParseExact(sO, "yyyy", null);


                // Search by status
                after_reports = after_reports.Where(e => e.status.ToLower().Trim() == sO.ToLower().Trim());

              

                // before_reports = before_reports.Where(e => e.before_ReportDate == ViewBag.Date); //.ToString().ToUpper().Contains(sO.ToUpper())
            }

            switch (sort)
            {
                case "report_Code_desc":
                    after_reports = after_reports.OrderByDescending(e => e.report_Code);
                    break;
                case "status":
                    after_reports = after_reports.OrderBy(e => e.status);
                    break;
                case "status_desc":
                    after_reports = after_reports.OrderByDescending(e => e.status);
                    break;
                default:
                    after_reports = after_reports.OrderBy(e => e.report_Code);
                    break;
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);

            return View(after_reports.ToPagedList(pageNumber, pageSize));
        }


        //
        // GET: /AfterReport/Details/5

        public ActionResult Details(int id = 0)
        {
            AfterReport afterreport = db.GetAfterReport(id);
            if (afterreport == null)
            {
                return HttpNotFound();
            }
            return View(afterreport);
        }

        //
        // GET: /AfterReport/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AfterReport/Create

        [HttpPost]
        public ActionResult Create(AfterReport afterreport)
        {
            if (ModelState.IsValid)
            {

                db.Save_After_Report(afterreport);
                return RedirectToAction("Index");
            }

            return View(afterreport);
        }

        //
        // GET: /AfterReport/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AfterReport afterreport = db.GetAfterReport(id);
            if (afterreport == null)
            {
                return HttpNotFound();
            }
            return View(afterreport);
        }

        //
        // POST: /AfterReport/Edit/5

        [HttpPost]
        public ActionResult Edit(AfterReport afterreport)
        {
            if (ModelState.IsValid)
            {

                db.Save_After_Report(afterreport);
                return RedirectToAction("Index");
            }
            return View(afterreport);
        }

        //
        // GET: /AfterReport/Delete/5

        public ActionResult Delete(int id = 0)
        {
            AfterReport afterreport = db.GetAfterReport(id);
            if (afterreport == null)
            {
                return HttpNotFound();
            }
            return View(afterreport);
        }

        //
        // POST: /AfterReport/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
           
            db.Delete_After_Report(id);
            return RedirectToAction("Index");
        }


        //
        //AJAX GET:
        public JsonResult GetBeforeReportShortInfo(int id)
        {
            AfterReport afReport = db.GetAfterReport(id);
            if (afReport == null)
            {
                return Json(new
                {
                    report_Code = "Not Found",
                    satus = "Not Found",
                    cause_of_fire = "Not Found",
                    //reportDate = "Not Found",
                });
            }
            return Json(new
            {
                report_Code = afReport.report_Code,
                satus = afReport.status,
                cause_of_fire = afReport.cause_of_fire,
                //reportComments = afReport.before_Report_Comments
            }, JsonRequestBehavior.AllowGet);

        }

        //
        //AJAX GET:
        public JsonResult GetMatchingAfterReports(string term)
        {
            var result = (from e in db.AfterReports
                          orderby e.after_ReportID
                          where e.report_Code.StartsWith(term)
                          select new { AfterReportID = e.after_ReportID, ReportCode = e.report_Code })
                          .Take(15).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}