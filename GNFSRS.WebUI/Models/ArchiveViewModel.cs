﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GNFSRS.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace GNFSRS.WebUI.Models
{
    public class ArchiveViewModel
    {
        private bool useDefault = false;
        [Display(Name="Enter Year to Archive")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy}", ApplyFormatInEditMode = true, NullDisplayText="Year")]
        public int? Year { get; set; }

        [Display(Name = "Use Default")]
        public bool? UseDefault
        {
            get;
            set;
        }
    }
}