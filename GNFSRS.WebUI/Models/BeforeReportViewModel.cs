﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GNFSRS.Domain.Entities;

namespace GNFSRS.WebUI.Models
{
    public class BeforeReportViewModel
    {
        public IEnumerable<BeforeReport> BeforeReports { get; set; }

        public PagingInfo PagingInfo { get; set; }

        public string CurrentCategory { get; set; }
    }
}