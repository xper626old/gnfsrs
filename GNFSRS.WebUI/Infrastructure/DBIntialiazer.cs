﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Concrete;
using System.Data.Entity;
using System.Data.Entity.Validation;


namespace GNFSRS.WebUI.Infrastructure
{
    public class DBIntialiazer : DropCreateDatabaseAlways<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            // Initialize Before Reports
            new List<BeforeReport>{
                new BeforeReport{ report_Code = "CodeE111", before_Name = "Mallam Atta Market", before_Phone = "02441133556", before_Venue = "Mallam Atta Market", 
                    before_Report_Comments = " A report of a fire outbreak in the Accra municipal market ", before_ReportDate = DateTime.Parse("2013-10-10"), before_ReportTime = DateTime.Parse( "08:00AM" ), tender_Depature_Time = DateTime.Parse( "08:15AM")},
                 new BeforeReport{ report_Code = "CodeE112", before_Name = "Makola  Market", before_Phone = "02441133545", before_Venue = "Makola Market", 
                    before_Report_Comments = " A report of a fire outbreak in the Accra municipal market ", before_ReportDate = DateTime.Parse("2013-10-10"), before_ReportTime = DateTime.Parse( "08:00AM" ), tender_Depature_Time = DateTime.Parse( "08:15AM")},
                 new BeforeReport{ report_Code = "CodeE113", before_Name = "Cantonments", before_Phone = "0544133556", before_Venue = "Cantoments Osu", 
                    before_Report_Comments = " A report of a fire outbreak in the Accra municipal market ", before_ReportDate = DateTime.Parse("2013-10-10"), before_ReportTime = DateTime.Parse( "08:00AM" ), tender_Depature_Time = DateTime.Parse( "08:15AM")},
                 new BeforeReport{ report_Code = "CodeE114", before_ReportDate = DateTime.Parse("10/8/2013"), tender_Depature_Time = DateTime.Parse(" 12:00:00 AM "), before_Venue =	"Kumasi", before_Name= "Oil Tank explosion", before_Phone =	"0244113554",
                     before_Report_Comments =	"An oil tanker exploded upon collision with a mini bus", before_ReportTime = DateTime.Parse("11:50 AM")},
            }.ForEach(b => context.BeforeReports.Add(b));

            // Initialize After Reports
            new List<AfterReport>{
                new AfterReport{ report_Code = "CodeE111", cause_of_fire = "Kerosene", number_of_casualties = 0, properties_lost = "20", return_time = DateTime.Parse("11:30 AM"), status = "Complete", type_of_fire = "Kerosene Fueld"},
                new AfterReport{ report_Code = "CodeE112", cause_of_fire = "Kerosene", number_of_casualties = 0, properties_lost = "10", return_time = DateTime.Parse("11:40 AM"), status = "Complete", type_of_fire = "Kerosene Fueld"},
                new AfterReport{ report_Code = "CodeE113", cause_of_fire = "Unknown", number_of_casualties = 0, properties_lost = "0", return_time = DateTime.Parse("11:30 AM"), status = "Complete", type_of_fire = "Unknown type"},
                new AfterReport{ report_Code = "CodeE114", cause_of_fire = "Petrol", number_of_casualties = 1, properties_lost = "0", return_time = DateTime.Parse("01:30 AM"), status = "Complete", type_of_fire = "Petrol Fueld"},
            }.ForEach(a => context.AfterReports.Add(a));

            // Initialize Years
            int CurrentYear = DateTime.Now.Year;

            for (int i = CurrentYear; i >= 1990; i--)
            {
                    context.Years.Add(new Years { Year = i });
            }
                
            //}
            base.Seed(context);
        }
    }
}