﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Moq;
using GNFSRS.Domain.Entities;
using GNFSRS.Domain.Abstract;
using GNFSRS.Domain.Concrete;
using System.Web.Routing;

namespace GNFSRS.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
                    ? null
                    : (IController)ninjectKernel.Get(controllerType);
        }


        private void AddBindings()
        {

            ninjectKernel.Bind<IBeforeReportRepository>().To<EFBeforeReportRepository>();
            ninjectKernel.Bind<IAfterReportRepository>().To<EFAfterReportRepository>();
            ninjectKernel.Bind<IArchiveRepository>().To<EFArchiveRepository>();
            ninjectKernel.Bind<IUserRepository>().To<EFUserRepository>();
            ninjectKernel.Bind<IEventImagesRepository>().To<EFEventImagesRepository>();
        }
    }
}